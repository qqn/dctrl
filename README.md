#### Secure messaging scorecard/Decentralized communication survey

##### *Note: This list is just in its brainstorming phase, gathering both projects and specs on which to compare them. There are many things to fix here; this note will disappear once all that's done. Until then, feedback is very appreciated (espcially via pull requests)!*

A comparison of all the secure, private, [free-as-in-beer, free-as-in-freedom,](https://en.wikipedia.org/wiki/Gratis_versus_libre#%22Free_beer%22_vs_%22freedom_of_speech%22_distinction) decentralized, open-source communication social networks, apps, programs and similar services out there in the search for the Holy Grail(s, ideally). A merging of a recent [blockchain SN survey](https://gitlab.com/qqn/dctrl/blob/master/Decentralized_Social_Networks_and_the_Blockchain.pdf) (itself stemming from my [cryptocurrency research](https://spmx.ca/btc)) with an old thread on [Telegram's security](https://diasp.org/posts/9090114), an older post analyzing [decentralized SNs](https://networkcultures.org/unlikeus/2012/12/07/breaking-down-the-walls-by-paul-sulzycki-unlike-us-reader), the EFF's (outdated) [Secure Messaging Scorecard](https://www.eff.org/node/82654) (and its [quasi-update](https://www.eff.org/deeplinks/2018/03/secure-messaging-more-secure-mess)), and current [Federation/Fediverse applications](https://medium.com/we-distribute/a-quick-guide-to-the-free-network-c069309f334) (as well as [Sean Tilley's other posts](https://medium.com/we-distribute))... and a small [Diaspora post](https://diasp.org/posts/10578326) discussing all this.

|	#	|	Service	|	Platform	|	Private	|	Secure	|	Decentralized	|	Federating	|
|	:-:	|	:-	|	:-:	|	:-:	|	:-:	|	:-:	|	:-:	
|	1	|	[Akasha](https://akasha.world/)											
|	2	|	[Basic Attention Token](https://basicattentiontoken.org/)											
|	3	|	[Bitmessage](https://bitmessage.org/)	|	-/a-/p-l	|	✓	|	✓	|	✓	|	✓	
|	4	|	[BitTube](https://bit.tube/)											
|	5	|	[Brair](https://briarproject.org/)											
|	6	|	[ChatSecure](https://chatsecure.org/)											
|	7	|	[Conversations](https://conversations.im/)											
|	8	|	[Diaspora](https://diasporafoundation.org/)	|	b/[a](https://f-droid.org/en/packages/com.github.dfa.diaspora_android/)-/pml	|		|		|	✓	|	✓	
|	9	|	[Discord](https://discordapp.com)	|	b/a-/pml									
|	10	|	[Everipedia](https://everipedia.org/)	|	b/--/---									
|	11	|	[Flowdock](https://www.flowdock.com/)											
|	12	|	[Friendica](https://friendi.ca/)	|	b/--/pml	|		|		|	✓	|	✓	
|	13	|	[Gab](https://gab.com/)											
|	14	|	[GangGo](https://ganggo.git.feneas.org/documentation/)											
|	15	|	[GitLab](https://gitlab.com/)											
|	16	|	[GNU social/StatusNet](https://www.gnu.org/software/social/)											
|	17	|	[Gogs](https://gogs.io/)											
|	18	|	[Hellofriend](https://joinhellofriend.com/)											
|	19	|	[Hike](https://hike.in/)											
|	20	|	[Hubzilla](https://project.hubzilla.org)	|		|		|		|	✓	|	✓	
|	21	|	[Indorse](https://indorse.io/)											
|	22	|	[Keybase](https://keybase.io/)	|	-/a-/pml	|	✓	|	✓	|		|	✓	
|	23	|	[Kik](https://www.kik.com/)											
|	24	|	[LBRY](https://lbry.io/)	|	-/a-/p--									
|	25	|	[Libertree](http://libertree.org/)	|	b/--/pml									
|	26	|	[Mastodon](https://mastodon.social/)	|	b/[a](https://tuskyapp.github.io/)-/pml	|		|		|	✓	|	✓	
|	27	|	[Mattermost](https://mattermost.com/)											
|	28	|	[MediaGoblin](https://mediagoblin.org/)											
|	29	|	[MeWe](https://mewe.com/)											
|	30	|	[Minds](https://www.minds.com/)											
|	31	|	[Movim](https://movim.eu/)											
|	32	|	[Nextcloud](https://nextcloud.com/)											
|	33	|	[Nexus](https://nexusearth.com/)											
|	34	|	[Obsidian](https://osm.obsidianplatform.com/)											
|	35	|	[Ono](https://www.ono.chat/)											
|	36	|	[OpenBazaar](https://openbazaar.org/)											
|	37	|	[Peepeth](https://peepeth.com/welcome)											
|	38	|	[Pidgin](https://www.pidgin.im/)											
|	39	|	[Primas](https://primas.io/)											
|	40	|	[Props](https://propsproject.com/)											
|	41	|	[Pump.io](http://pump.io/)											
|	42	|	[Retroshare](http://retroshare.net/)											
|	43	|	[Ricochet](https://ricochet.im/)	|	-/a-/pml	|	✓	|	✓	|	✓	|	✓	
|	44	|	[Ring](https://ring.cx/)	|	-/a-/pml	|	✓	|	✓	|	✓	|	✓	
|	45	|	[Riot/Matrix](https://matrix.org/docs/projects/client/riot.html)	|	-/a-/pml	|	✓	|	✓	|	✓			
|	46	|	[Rocketchat](https://rocket.chat/)											
|	47	|	[Sapien](https://www.sapien.network/)											
|	48	|	[SecureScuttlebutt](https://www.scuttlebutt.nz/)	|	b/[a](https://www.manyver.se/)-/pml	|		|		|	✓	|	NO	
|	49	|	[Signal](https://signal.org/)	|	-/a-/pml	|	✓	|		|	NO	|	NO	
|	50	|	[Socialhome](https://socialhome.network/)											
|	51	|	[Sola](https://sola.ai/)											
|	52	|	[SoMee](https://somee.social/)											
|	53	|	[Spectrum](https://spectrum.chat/)											
|	54	|	[Status.im](https://dev.status.im/)											
|	55	|	[Steemit](https://steemit.com/)	|	b/--/---									
|	56	|	[Synereo](https://synereo.com/)											
|	57	|	[Telegram](https://telegram.org/)	|	-/a-/pml	|	NO	|		|	NO	|	NO	
|	58	|	[Tox](https://tox.chat/)											
|	59	|	[Upfiring](https://upfiring.com/)											
|	60	|	[Voat](https://voat.co/)											
|	61	|	[Wickr](https://wickr.com/)											
|	62	|	[Wire](https://wire.com/)											
|	63	|	[WriteFreely](https://writefreely.org/)											
|	64	|	[Y'alls](https://yalls.org/)											
|	65	|	[Yours](https://www.yours.org/)											
|	66	|	[Zulip](https://zulipchat.com/)											

**Scores**: blank (unknown), beta (working on it), ✓ (score!), NO (fail)  
*Platform*: format = browser/mobile/desktop, where browser = chrome, firefox, safari, opera, brave, explorer/edge & mobile = android/iOS & desktop = PC/Mac/Linux (eg: offer all shows b/ai/pml)
*Secure*: Encrypted decentralized and/or local data storage  
*Private*: Default encrypted across ALL channels  
*Decentralized*: Distributed across many servers, ideally P2P  
*Federating*: Communicating with at least one other service on this list  

Popular services not included: anything from Facebook/Google/Microsoft (eg. Instagram, WhatsApp, Gmail/Gchat/Hangouts/Allo, Skype, GitHub, LinkedIn) or WeChat because [surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism), anything with a paywall/block (Pinterest, LinkedIn again), anything requiring payment (eg: [Threema](https://threema.ch) or [freemium](https://en.wikipedia.org/wiki/Freemium) services like [Slack](https://slack.com/), where old messages are locked up and released only after buying a subscription).

To do:
1. Add sources for all table scores
2. Figure out where XMPP/Jabber/IRC/ (and PGP) stands in this list
3. Include services like [Appear.in](https://appear.in/), [Cisco Spark/Webex](https://teams.webex.com/signin), [Jitsi Meet](https://meet.jit.si/), or [Talky](https://talky.io/) (or defunct services lik [Firefox Hello](https://support.mozilla.org/en-US/kb/hello-status))?
4. Include characteristics from https://www.picflash.org/viewer.php?img=p9NCOGXNV05TY.png
5. Where do filesharing services like [IPFS](https://ipfs.io/), [Peergos](https://peergos.org/) and [ownCloud](https://owncloud.org/) fit?
6. Integrate information from [secushare](https://secushare.org/comparison), [EDN](https://wiki.c3d2.de/EDN), the  [dSN](https://en.wikipedia.org/wiki/Distributed_social_network) & [dSN software comparison](https://en.wikipedia.org/wiki/Comparison_of_software_and_protocols_for_distributed_social_networking) wikis, the [W3C](https://www.w3.org/blog/news/archives/3958), the [Bitmessage FAQ](https://bitmessage.org/wiki/FAQ#How_does_Bitmessage_compare_to_other_messaging_methods), and [this Manyverse blog post](https://staltz.com/early-days-in-the-manyverse.html).
7. Divide list into "categories" of networks, perhaps w/what current services they're displacing
8. Consider blockchain SN implementation difficulties presented by the [IEEE](https://spectrum.ieee.org/tech-talk/computing/networks/3-obstacles-to-moving-social-media-platforms-to-a-blockchain)
9. Integrate information from [this post on the fediverse](https://gist.github.com/MasterOfTheTiger/f98845c9e1b9f3c172926119474f21f4/) and [this conversation on [WriteFreely](https://news.ycombinator.com/item?id=18446278)